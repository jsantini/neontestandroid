package com.testneon.model.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jsantini on 04/09/17.
 */

public class SendMoneyDTO {

    @SerializedName("ClienteId")
    private long clientId;
    private String token;
    @SerializedName("valor")
    private double value;

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
