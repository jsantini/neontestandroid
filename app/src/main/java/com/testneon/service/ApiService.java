package com.testneon.service;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.testneon.model.dto.ResponseGetTransferDTO;
import com.testneon.model.dto.SendMoneyDTO;

import java.io.IOException;
import java.util.List;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by jsantini on 04/09/17.
 */

public class ApiService {

    private static NeonApi neonApi;
    private static String URL_BASE = "http://processoseletivoneon.azurewebsites.net";

    public static NeonApi getApi() {
        OkHttpClient okClient = new OkHttpClient();
        okClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = chain.request().newBuilder();
                requestBuilder.header("Content-Type", "application/json");
                requestBuilder.method(original.method(), original.body());
                return chain.proceed(requestBuilder.build());
            }
        });
        Retrofit client = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                .client(okClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        neonApi = client.create(NeonApi.class);
        return neonApi;
    }

    public interface NeonApi {

        @GET("/GenerateToken")
        Call<String> getToken(@Query("nome") String nome, @Query("email") String email);

        @GET("/GetTransfers")
        Call<List<ResponseGetTransferDTO>> getTransfer(@Query("token") String token);

        @POST("/SendMoney")
        Call<Object> sendMoney(@Body SendMoneyDTO sendMoneyDTO);
    }
}
