package com.testneon.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.testneon.application.NeonApplication;

/**
 * Created by jsantini on 04/09/17.
 */

public class ServiceUtil {

    /**
     * Checks whether there's an active internet connection at the current moment.
     * @return TRUE if there's internet, FALSE if you're offline.
     */
    public static boolean isConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) NeonApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
