package com.testneon.ui.main;

import android.content.Context;

import com.testneon.ui.IView;

/**
 * Created by jsantini on 02/09/17.
 */

public interface IMainView extends IView {

    void setTvProfileName(String name);
    void setTvProfileEmail(String email);

    class MainEmptyView implements IMainView {

        @Override
        public void showLoading(String msg) {

        }

        @Override
        public void hideLoading() {

        }

        @Override
        public void showGenericError(String msg) {

        }

        @Override
        public void setTvProfileName(String name) {

        }

        @Override
        public void setTvProfileEmail(String email) {

        }
    }
}
