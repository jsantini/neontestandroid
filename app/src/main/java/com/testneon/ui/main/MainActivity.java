package com.testneon.ui.main;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.testneon.R;
import com.testneon.ui.BaseActivity;
import com.testneon.ui.contacts.ContactsActivity;
import com.testneon.ui.historic.HistoricActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements IMainView {

    private MainPresenter presenter;

    @BindView(R.id.tv_profile_name)
    TextView tvProfileName;
    @BindView(R.id.tv_profile_email)
    TextView tvProfileEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter = new MainPresenter(this);
        presenter.getToken();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.setView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.clearView();
    }

    @Override
    public void setTvProfileName(final String name) {
        tvProfileName.setText(name);
    }

    @Override
    public void setTvProfileEmail(final String email) {
        tvProfileEmail.setText(email);
    }

    @OnClick(R.id.bt_send_money)
    public void onClickSendMoney(final View view) {
        startActivity(ContactsActivity.getStartIntent(this));
    }

    @OnClick(R.id.bt_historic)
    public void onClickHistoric(final View view) {
        startActivity(HistoricActivity.getStartIntent(this));
    }
}
