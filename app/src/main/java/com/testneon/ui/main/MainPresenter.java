package com.testneon.ui.main;

import android.content.Context;

import com.testneon.R;
import com.testneon.service.ApiService;
import com.testneon.service.ServiceUtil;
import com.testneon.ui.IPresenter;
import com.testneon.ui.IView;
import com.testneon.ui.util.Util;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by jsantini on 02/09/17.
 */

public class MainPresenter implements IPresenter {

    private IMainView mView;
    private final String nome = "Jean Santini";
    private final String email = "jean.ferreira.santini@gmail.com";
    private Context mContext;

    public MainPresenter(final Context context) {
        this.mContext = context;
    }

    @Override
    public void setView(final IView view) {
        this.mView = (IMainView) view;
        initDataProfile();
    }

    @Override
    public void clearView() {
        this.mView = new IMainView.MainEmptyView();
    }

    public void initDataProfile() {
        mView.setTvProfileName(nome);
        mView.setTvProfileEmail(email);
    }

    public void getToken() {
        if(ServiceUtil.isConnected()) {
            ApiService.NeonApi service = ApiService.getApi();

            Call<String> call = service.getToken(nome, email);

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Response<String> response, Retrofit retrofit) {
                    if(response.body() != null) {
                        Util.saveOrUpdateTokenInPreferences(mContext, response.body());
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                }
            });
        }
    }
}
