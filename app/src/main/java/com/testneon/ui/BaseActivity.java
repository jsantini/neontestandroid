package com.testneon.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.testneon.R;
import com.testneon.ui.main.MainActivity;
import com.testneon.ui.dialog.LoadingDialog;
import com.testneon.ui.dialog.MessageDialog;
import com.testneon.ui.util.Util;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

public class BaseActivity extends AppCompatActivity implements MessageDialog.OnModalDialogListener {

    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        overridePendingTransition(R.anim.slide_in_rigth_to_left, R.anim.slide_out_rigth_to_left);
    }

    public void showLoading(final String msg) {
        Util.hideKeyboard(this);
        hideLoading();
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        loadingDialog = LoadingDialog.newInstance(msg);
        loadingDialog.setCancelable(false);
        loadingDialog.show(ft, "dialog");
    }

    public void hideLoading() {
        if(loadingDialog != null) {
            loadingDialog.dismiss();
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left_to_rigth, R.anim.slide_out_left_to_rigth);
    }

    public void showGenericError(final String msg) {
        hideLoading();
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        MessageDialog messageDialog = MessageDialog.newInstance(msg, this, MessageDialog.TYPE_ERROR);
        messageDialog.setCancelable(false);
        messageDialog.show(ft, "dialog");
    }

    @Override
    public void onModalDialogOKClick() {
        startActivity(new Intent(this, MainActivity.class));
    }
}
