package com.testneon.ui.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.testneon.model.domain.Contact;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by jsantini on 03/09/17.
 */

public class Util {
    public static final String SHARED_PREFS_IDENTIFIER = "com.neon";
    public static final String KEY_NEON_TOKEN = "neon_token";


    public static double converterCurrencyValue(final String value) {
        String newNumber = value.replace("R$", "");
        return Double.parseDouble(replaceCommaByPoint(newNumber));
    }

    public static String replaceCommaByPoint(final String value) {
        String newNumber = value.replace(".", "").replace(",", ".");
        return newNumber;
    }

    public static void saveOrUpdateTokenInPreferences(final Context context, final String token) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS_IDENTIFIER, context.MODE_PRIVATE);
        sharedPreferences.edit().putString(KEY_NEON_TOKEN, token).apply();
    }

    public static String getToken(final Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS_IDENTIFIER, context.MODE_PRIVATE);
        String value = sharedPreferences.getString(KEY_NEON_TOKEN, "");
        return value;
    }

    public static void hideKeyboard(Activity activity) {
        if (activity != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            View view = activity.getCurrentFocus();

            if (null != view && null != view.getWindowToken() && EditText.class.isAssignableFrom(view.getClass())) {
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            } else {
                activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        }
    }

    public static List<Contact> loadContacts() {
        List<Contact> contactList = new ArrayList<Contact>();
        Contact contact = new Contact();
        contact.setId(1);
        contact.setName("Matheus Alves");
        contact.setPhone("(11) 98765-4321");
        contact.setPicture("https://goo.gl/i1UnSV");

        Contact contact1 = new Contact();
        contact1.setId(2);
        contact1.setName("Angela Molano");
        contact1.setPhone("(11) 95462-4321");
        contact1.setPicture("https://goo.gl/iaUT8Z");

        Contact contact2 = new Contact();
        contact2.setId(3);
        contact2.setName("Felipe Miranda");
        contact2.setPhone("(11) 94731-4321");
        contact2.setPicture("https://goo.gl/Tk3U5J");

        Contact contact3 = new Contact();
        contact3.setId(4);
        contact3.setName("Carla Mendonça");
        contact3.setPhone("(11) 91122-4321");
        contact3.setPicture("https://goo.gl/Z98HM4");

        Contact contact4 = new Contact();
        contact4.setId(5);
        contact4.setName("Josias Reis");
        contact4.setPhone("(11) 91717-4321");
        contact4.setPicture("https://s26.postimg.org/noluxnjuh/profile.jpg");

        Contact contact5 = new Contact();
        contact5.setId(6);
        contact5.setName("Fabiano Silva");
        contact5.setPhone("(11) 91745-4321");
        contact5.setPicture("https://s26.postimg.org/noluxnjuh/profile.jpg");

        Contact contact6 = new Contact();
        contact6.setId(7);
        contact6.setName("Lorena Faria");
        contact6.setPhone("(11) 97184-4321");
        contact6.setPicture("https://s26.postimg.org/noluxnjuh/profile.jpg");

        Contact contact7 = new Contact();
        contact7.setId(8);
        contact7.setName("Bianca Tavares");
        contact7.setPhone("(11) 91744-4321");
        contact7.setPicture("https://goo.gl/Z98HM4");

        Contact contact8 = new Contact();
        contact8.setId(9);
        contact8.setName("Ludmila Pereira");
        contact8.setPhone("(11) 90045-4321");
        contact8.setPicture("https://s26.postimg.org/noluxnjuh/profile.jpg");

        Contact contact9 = new Contact();
        contact9.setId(10);
        contact9.setName("Camila Fonseca");
        contact9.setPhone("(11) 91047-4321");
        contact9.setPicture("https://s26.postimg.org/noluxnjuh/profile.jpg");

        Contact contact10 = new Contact();
        contact10.setId(11);
        contact10.setName("Maria Mercedez");
        contact10.setPhone("(11) 98765-4321");
        contact10.setPicture("https://s26.postimg.org/noluxnjuh/profile.jpg");

        Contact contact11 = new Contact();
        contact11.setId(12);
        contact11.setName("João Guerra");
        contact11.setPhone("(11) 98765-4321");
        contact11.setPicture("https://s26.postimg.org/noluxnjuh/profile.jpg");

        Contact contact12 = new Contact();
        contact12.setId(13);
        contact12.setName("Pablo Araújo");
        contact12.setPhone("(11) 90466-4321");
        contact12.setPicture("https://s26.postimg.org/noluxnjuh/profile.jpg");

        Contact contact13 = new Contact();
        contact13.setId(14);
        contact13.setName("Danilo Alexandre");
        contact13.setPhone("(11) 98765-4321");
        contact13.setPicture("https://s26.postimg.org/noluxnjuh/profile.jpg");

        Contact contact14 = new Contact();
        contact14.setId(15);
        contact14.setName("Pedro Ivo");
        contact14.setPhone("(11) 98765-4321");
        contact14.setPicture("https://s26.postimg.org/noluxnjuh/profile.jpg");

        contactList.add(contact);
        contactList.add(contact1);
        contactList.add(contact2);
        contactList.add(contact3);
        contactList.add(contact4);
        contactList.add(contact5);
        contactList.add(contact6);
        contactList.add(contact7);
        contactList.add(contact8);
        contactList.add(contact9);
        contactList.add(contact10);
        contactList.add(contact11);
        contactList.add(contact12);
        contactList.add(contact13);
        contactList.add(contact14);

        Collections.sort(contactList, new Comparator<Contact>() {
            @Override
            public int compare(Contact c1, Contact c2) {
                return c1.getName().compareTo(c2.getName());
            }
        });
        return contactList;
    }

    public static String formatToCurrency(double value, boolean returnCurrency) {
        String valorString = "";
        try {
            Locale ptBr = new Locale("pt", "BR");
            valorString = NumberFormat.getCurrencyInstance(ptBr).format(value);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (!returnCurrency) {
            valorString = valorString.replace("R$", "");
        } else {
            valorString = valorString.replace("R$", "R$ ");
        }
        return valorString;
    }

    public static Date converterStringToDate(final String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            return null;
        }
        return convertedDate;
    }
}
