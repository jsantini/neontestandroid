package com.testneon.ui.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.testneon.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jsantini on 02/09/17.
 */

public class ContactsViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.content_item_contact)
    public LinearLayout llContentItemContact;
    @BindView(R.id.tv_name)
    public TextView tvName;
    @BindView(R.id.tv_phone)
    public TextView tvPhone;
    @BindView(R.id.iv_picture)
    public ImageView ivPicture;
    @BindView(R.id.tv_value_transfer)
    public TextView tvValueTransfer;

    public ContactsViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
