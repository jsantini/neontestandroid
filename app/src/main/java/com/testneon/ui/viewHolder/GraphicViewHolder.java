package com.testneon.ui.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.testneon.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jsantini on 03/09/17.
 */

public class GraphicViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.view_line)
    public View viewLine;
    @BindView(R.id.iv_picture)
    public ImageView ivPicture;
    @BindView(R.id.tv_value)
    public TextView tvValue;

    public GraphicViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
