package com.testneon.ui;

import android.content.Context;

/**
 * Created by jsantini on 02/09/17.
 */

public interface IView {

    void showLoading(String msg);
    void hideLoading();

    void showGenericError(String msg);
}
