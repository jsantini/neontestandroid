package com.testneon.ui.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.testneon.R;
import com.testneon.model.domain.Contact;
import com.testneon.ui.util.NumberTextWatcher;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jsantini on 03/09/17.
 */

public class SendMoneyDialog extends DialogFragment {

    private Contact contact;
    private Context mContext;
    @BindView(R.id.et_value)
    EditText etValue;
    @BindView(R.id.bt_modal_send_money)
    Button btSendMoney;
    @BindView(R.id.iv_modal_contact_picture)
    ImageView ivPicture;
    @BindView(R.id.tv_modal_name)
    TextView tvModalName;
    @BindView(R.id.tv_modal_phone)
    TextView tvModalPhone;
    @BindView(R.id.iv_close)
    ImageView ivClose;

    private OnSendMoneyModalListener sendMoneyModalListener;

    public static SendMoneyDialog newInstance(final String jsonContact,
                                              final OnSendMoneyModalListener sendMoneyModalListener) {
        SendMoneyDialog f = new SendMoneyDialog();
        f.contact = new Gson().fromJson(jsonContact, Contact.class);
        f.sendMoneyModalListener = sendMoneyModalListener;
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.modal_send_money, container, false);
        mContext = rootView.getContext();
        ButterKnife.bind(this, rootView);
        initDialog();
        return rootView;
    }

    private void initDialog() {
        Dialog d = getDialog();
        if (d != null) {
            d.requestWindowFeature(Window.FEATURE_NO_TITLE);
            d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        etValue.addTextChangedListener(new NumberTextWatcher(etValue));
        if(sendMoneyModalListener != null) {
            btSendMoney.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sendMoneyModalListener.onSendMoneyOnClick(etValue.getText().toString());
                }
            });
        }
        Picasso.with(mContext).load(contact.getPicture()).placeholder(R.drawable.noimage)
                .into(ivPicture);
        tvModalName.setText(contact.getName());
        tvModalPhone.setText(contact.getPhone());
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    public interface OnSendMoneyModalListener {
        void onSendMoneyOnClick(String valueStr);
    }

}
