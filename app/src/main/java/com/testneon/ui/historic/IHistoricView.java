package com.testneon.ui.historic;

import com.testneon.model.domain.Contact;
import com.testneon.ui.IView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jsantini on 04/09/17.
 */

public interface IHistoricView extends IView {

    void showChartTransfers(List<Contact> contacts);

    void showContactsTransferList(ArrayList<Contact> contactsTransfer);

    class HistoricEmptyView implements IHistoricView {


        @Override
        public void showLoading(String msg) {

        }

        @Override
        public void hideLoading() {

        }

        @Override
        public void showGenericError(String msg) {

        }

        @Override
        public void showChartTransfers(List<Contact> contacts) {

        }

        @Override
        public void showContactsTransferList(ArrayList<Contact> contactsTransfer) {

        }
    }
}
