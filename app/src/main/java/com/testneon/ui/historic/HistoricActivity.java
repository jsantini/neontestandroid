package com.testneon.ui.historic;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.testneon.R;
import com.testneon.model.domain.Contact;
import com.testneon.ui.BaseActivity;
import com.testneon.ui.adapter.ContactsAdapter;
import com.testneon.ui.adapter.ChartAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoricActivity extends BaseActivity implements IHistoricView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.rv_graphics)
    RecyclerView rvGraphics;
    @BindView(R.id.rv_contacts_historic)
    RecyclerView rvContactsHistoric;
    private ChartAdapter chartAdapter;
    private ContactsAdapter contactsAdapter;

    private HistoricPresenter presenter;

    public static Intent getStartIntent(final Context context) {
        return new Intent(context, HistoricActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historic);
        ButterKnife.bind(this);
        presenter = new HistoricPresenter(this);
        presenter.setView(this);
        setupToolBar();
        setupList();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.setView(this);
        presenter.getTransfers();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.clearView();
    }

    public void setupToolBar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            tvToolbarTitle.setText(getResources().getString(R.string.activity_historic_title));
            toolbar.setNavigationIcon(R.drawable.ic_chevron_left_white_48dp);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

    private void setupList() {
        chartAdapter = new ChartAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvGraphics.setLayoutManager(layoutManager);
        rvGraphics.setAdapter(chartAdapter);

        contactsAdapter = new ContactsAdapter(this);
        rvContactsHistoric.setLayoutManager(new LinearLayoutManager(this));
        rvContactsHistoric.setAdapter(contactsAdapter);
    }

    @Override
    public void showChartTransfers(List<Contact> contacts) {
        chartAdapter.setContacts(contacts);
    }

    @Override
    public void showContactsTransferList(ArrayList<Contact> contactsTransfer) {
        contactsAdapter.setContactList(contactsTransfer);
    }
}
