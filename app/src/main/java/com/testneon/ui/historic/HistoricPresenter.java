package com.testneon.ui.historic;

import android.content.Context;
import android.util.Log;

import com.testneon.R;
import com.testneon.model.domain.Contact;
import com.testneon.service.ApiService;
import com.testneon.service.ServiceUtil;
import com.testneon.model.dto.ResponseGetTransferDTO;
import com.testneon.ui.IPresenter;
import com.testneon.ui.IView;
import com.testneon.ui.util.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by jsantini on 04/09/17.
 */

public class HistoricPresenter implements IPresenter {

    private IHistoricView mView;
    private ArrayList<Contact> contactsTransfersSummarized;
    private ArrayList<Contact> contactsTransfer;
    private Context mContext;

    public HistoricPresenter(final Context context) {
        this.mContext = context;
    }

    @Override
    public void setView(final IView view) {
        this.mView = (IHistoricView) view;
    }

    @Override
    public void clearView() {
        this.mView = new IHistoricView.HistoricEmptyView();
    }

    public void getTransfers() {
        if(ServiceUtil.isConnected()) {
            mView.showLoading(mContext.getString(R.string.loading_historic));
            ApiService.NeonApi service = ApiService.getApi();
            final String token = Util.getToken(mContext);
            Call<List<ResponseGetTransferDTO>> call = service.getTransfer(token);
            call.enqueue(new Callback<List<ResponseGetTransferDTO>>() {
                @Override
                public void onResponse(Response<List<ResponseGetTransferDTO>> response, Retrofit retrofit) {
                    for(long i=0; i < 9999; i++) {
                        Log.d("jean", "jean : " + i);
                    }
                    mView.hideLoading();
                    if(response.code() >= 200 && response.code() < 300) {
                        if (response.body() != null) {
                            List<ResponseGetTransferDTO> transferDTOs = response.body();
                            summarizeTransfers(transferDTOs);
                            orderTransfers(transferDTOs);
                            mView.showContactsTransferList(contactsTransfer);
                            mView.showChartTransfers(contactsTransfersSummarized);
                        }
                    } else {
                        mView.showGenericError(mContext.getString(R.string.generic_error));
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    mView.showGenericError(mContext.getString(R.string.no_connection));
                }
            });
        } else {
            mView.showGenericError(mContext.getString(R.string.no_connection));
        }
    }

    private void summarizeTransfers(final List<ResponseGetTransferDTO> responseGetTransferDTOs) {
        List<Contact> contacts = Util.loadContacts();
        contactsTransfersSummarized = new ArrayList<Contact>();
        for(ResponseGetTransferDTO dto:responseGetTransferDTOs) {
            Contact contact = new Contact();
            contact.setId(dto.getClientId());
            boolean hasInserted = false;
            if(contactsTransfersSummarized.contains(contact)) {
                hasInserted = true;
                contact = contactsTransfersSummarized.get(contactsTransfersSummarized.indexOf(contact));
            } else {
                contact = contacts.get(contacts.indexOf(contact));
            }
            contact.setValue(contact.getValue() + dto.getValue());
            if(!hasInserted) {
                contactsTransfersSummarized.add(contact);
            }
        }
        Collections.sort(contactsTransfersSummarized, Collections.<Contact>reverseOrder(new Comparator<Contact>() {
            @Override
            public int compare(Contact c1, Contact c2) {
                return Double.compare(c1.getValue(), c2.getValue());
            }
        }));
    }

    private void orderTransfers(final List<ResponseGetTransferDTO> responseGetTransferDTOs) {
        List<Contact> contacts = Util.loadContacts();
        contactsTransfer = new ArrayList<Contact>();
        for(ResponseGetTransferDTO dto:responseGetTransferDTOs) {
            Contact contact = new Contact();
            contact.setId(dto.getClientId());
            if(contacts.contains(contact)) {
                Contact contactTemp = contacts.get(contacts.indexOf(contact));
                contact.setName(contactTemp.getName());
                contact.setPicture(contactTemp.getPicture());
                contact.setPhone(contactTemp.getPhone());
            }
            contact.setDate(Util.converterStringToDate(dto.getData()));
            contact.setValue(dto.getValue());
            contactsTransfer.add(contact);
        }

        Collections.sort(contactsTransfer, Collections.reverseOrder(new Comparator<Contact>() {
            @Override
            public int compare(Contact c1, Contact c2) {
                return c1.getDate().compareTo(c2.getDate());
            }
        }));
    }
}
