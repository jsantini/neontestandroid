package com.testneon.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.testneon.R;
import com.testneon.model.domain.Contact;
import com.testneon.ui.util.Util;
import com.testneon.ui.viewHolder.ContactsViewHolder;

import java.util.List;

/**
 * Created by jsantini on 02/09/17.
 */

public class ContactsAdapter extends RecyclerView.Adapter<ContactsViewHolder> {

    private Context mContext;
    private List<Contact> contactList;
    private OnSelectContactListener onSelectContactListener;

    public ContactsAdapter(final Context context) {
        this.mContext = context;
    }

    @Override
    public ContactsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false);
        this.mContext = view.getContext();
        return new ContactsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactsViewHolder holder, int position) {
        final Contact contact = contactList.get(position);
        holder.tvName.setText(contact.getName());
        holder.tvPhone.setText(contact.getPhone());
        Picasso.with(mContext).load(contact.getPicture()).placeholder(R.drawable.noimage)
                .into(holder.ivPicture);

        if(onSelectContactListener != null) {
            holder.llContentItemContact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onSelectContactListener.onSelectContact(new Gson().toJson(contact));
                }
            });
        }
        if(contact.getValue() > 0) {
            holder.tvValueTransfer.setText(Util.formatToCurrency(contact.getValue(), true));
            holder.tvValueTransfer.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        if(contactList == null) {
            return 0;
        }
        return contactList.size();
    }

    public void setContactList(final List<Contact> contactList) {
        this.contactList = contactList;
        notifyDataSetChanged();
    }

    public interface OnSelectContactListener {
        void onSelectContact(String contactJson);
    }

    public void setOnSelectContactListener(final OnSelectContactListener onSelectContactListener) {
        this.onSelectContactListener = onSelectContactListener;
    }
}
