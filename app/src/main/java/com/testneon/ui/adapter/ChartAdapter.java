package com.testneon.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.testneon.R;
import com.testneon.model.domain.Contact;
import com.testneon.ui.util.Util;
import com.testneon.ui.viewHolder.GraphicViewHolder;

import java.util.List;

/**
 * Created by jsantini on 03/09/17.
 */

public class ChartAdapter extends RecyclerView.Adapter<GraphicViewHolder> {
    private Context mContext;
    private List<Contact> contacts;
    private double maxValue;

    @Override
    public GraphicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_graphic, parent, false);
        this.mContext = view.getContext();
        return new GraphicViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if(contacts == null) {
            return 0;
        }
        return contacts.size();
    }

    @Override
    public void onBindViewHolder(GraphicViewHolder holder, int position) {
        Contact contact = contacts.get(position);
        if(position == 0) {
            maxValue = contact.getValue();
        }
        double currentValue = contact.getValue();
        int newHeight = (int) ((currentValue * 220) / maxValue);
        holder.tvValue.setText(Util.formatToCurrency(contact.getValue(), false));
        Picasso.with(mContext).load(contact.getPicture()).placeholder(R.drawable.noimage)
                .into(holder.ivPicture);

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) holder.viewLine.getLayoutParams();
        RelativeLayout.LayoutParams newParams = new RelativeLayout.LayoutParams(layoutParams.width, newHeight);
        newParams.addRule(RelativeLayout.ABOVE, holder.ivPicture.getId());
        newParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        holder.viewLine.setLayoutParams(newParams);
    }

    public void setContacts(final List<Contact> contacts) {
        this.contacts = contacts;
        notifyDataSetChanged();
    }
}
