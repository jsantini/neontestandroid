package com.testneon.ui.contacts;

import com.testneon.model.domain.Contact;
import com.testneon.ui.IView;

import java.util.List;

/**
 * Created by jsantini on 02/09/17.
 */

public interface IContactsView extends IView {

    void showContacts(List<Contact> contactList);

    void showSuccessSendMoney();

    void showMinValueError();

    class ContactsEmptyView implements IContactsView {

        @Override
        public void showLoading(String msg) {

        }

        @Override
        public void hideLoading() {

        }

        @Override
        public void showGenericError(String msg) {

        }

        @Override
        public void showContacts(List<Contact> contactList) {

        }

        @Override
        public void showSuccessSendMoney() {

        }

        @Override
        public void showMinValueError() {

        }
    }
}
