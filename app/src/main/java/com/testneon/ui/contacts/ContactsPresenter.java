package com.testneon.ui.contacts;

import android.content.Context;

import com.google.gson.Gson;
import com.testneon.R;
import com.testneon.model.domain.Contact;
import com.testneon.service.ApiService;
import com.testneon.service.ServiceUtil;
import com.testneon.model.dto.SendMoneyDTO;
import com.testneon.ui.IPresenter;
import com.testneon.ui.IView;
import com.testneon.ui.util.Util;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by jsantini on 02/09/17.
 */

public class ContactsPresenter implements IPresenter {

    private IContactsView mView;
    private Contact selectedContact;
    private Context mContext;

    public ContactsPresenter(final Context context) {
        this.mContext = context;
    }

    @Override
    public void setView(final IView view) {
        this.mView = (IContactsView) view;
    }

    @Override
    public void clearView() {
        this.mView = new IContactsView.ContactsEmptyView();
    }

    public void loadContacts() {
        mView.showContacts(Util.loadContacts());
    }

    public void sendMoney(String valueStr) {
        if(ServiceUtil.isConnected()) {
            mView.showLoading(mContext.getString(R.string.loading_send_money));
            ApiService.NeonApi service = ApiService.getApi();
            if(valueStr == null || valueStr.equals("")) {
                mView.showMinValueError();
                return;
            }
            double value = Util.converterCurrencyValue(valueStr);
            if(value <= 0) {
                mView.showMinValueError();
                return;
            }
            Call<Object> call = service.sendMoney(buildSendMoneyDTO(value));

            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Response<Object> response, Retrofit retrofit) {
                    mView.hideLoading();
                    if(response.code() >= 200 && response.code() < 300) {
                        if (response.body() != null) {
                            mView.showSuccessSendMoney();
                        }
                    } else {
                        mView.showGenericError(mContext.getString(R.string.generic_error));
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    mView.showGenericError(mContext.getString(R.string.generic_error));
                }
            });
        } else {
            mView.showGenericError(mContext.getString(R.string.no_connection));
        }

    }

    private SendMoneyDTO buildSendMoneyDTO(double value) {
        SendMoneyDTO sendMoneyDTO = new SendMoneyDTO();
        sendMoneyDTO.setClientId(selectedContact.getId());
        sendMoneyDTO.setToken(Util.getToken(mContext));
        sendMoneyDTO.setValue(value);
        return sendMoneyDTO;
    }

    public void setSelectedContact(final String selectedContact) {
        this.selectedContact = new Gson().fromJson(selectedContact, Contact.class);
    }
}
