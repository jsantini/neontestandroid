package com.testneon.ui.contacts;

import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.testneon.R;
import com.testneon.model.domain.Contact;
import com.testneon.ui.adapter.ContactsAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactsFragment extends Fragment {


    private Context mContext;
    @BindView(R.id.rv_contacts)
    RecyclerView rvContacts;
    private ContactsAdapter contactsAdapter;
    private ContactsPresenter presenter;
    private ContactsAdapter.OnSelectContactListener onSelectContactListener;

    public ContactsFragment() {
        // Required empty public constructor
    }

    public static ContactsFragment newInstance(final ContactsAdapter.OnSelectContactListener onSelectContactListener) {
        ContactsFragment fragment = new ContactsFragment();
        fragment.onSelectContactListener = onSelectContactListener;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contacts, container, false);
        mContext = view.getContext();
        ButterKnife.bind(this, view);
        setupList();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.loadContacts();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setupList() {
        contactsAdapter = new ContactsAdapter(mContext);
        contactsAdapter.setOnSelectContactListener(onSelectContactListener);
        rvContacts.setLayoutManager(new LinearLayoutManager(mContext));
        rvContacts.setAdapter(contactsAdapter);

    }

    public void setPresenter(final ContactsPresenter presenter) {
        this.presenter = presenter;
    }

    public void showContacts(final List<Contact> contactList) {
        contactsAdapter.setContactList(contactList);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
