package com.testneon.ui.contacts;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.testneon.R;
import com.testneon.model.domain.Contact;
import com.testneon.ui.BaseActivity;
import com.testneon.ui.adapter.ContactsAdapter;
import com.testneon.ui.dialog.MessageDialog;
import com.testneon.ui.dialog.SendMoneyDialog;
import com.testneon.ui.util.Util;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactsActivity extends BaseActivity implements IContactsView, ContactsAdapter.OnSelectContactListener, SendMoneyDialog.OnSendMoneyModalListener, MessageDialog.OnModalDialogListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView tvToolbarTitle;

    @BindView(R.id.content_contacts)
    LinearLayout contentContacts;

    private ContactsFragment contactsFragment;
    private ContactsPresenter presenter;
    private SendMoneyDialog sendMoneyDialog;

    public static Intent getStartIntent(final Context context) {
        Intent intent = new Intent(context, ContactsActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        ButterKnife.bind(this);
        presenter = new ContactsPresenter(this);
        presenter.setView(this);
        setupToolBar();
        setupContactsFragment();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.setView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.clearView();
    }

    public void setupToolBar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            tvToolbarTitle.setText(getResources().getString(R.string.activity_contacts_title));
            toolbar.setNavigationIcon(R.drawable.ic_chevron_left_white_48dp);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

    private void setupContactsFragment() {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        contactsFragment = ContactsFragment.newInstance(this);
        contactsFragment.setPresenter(presenter);
        ft.add(R.id.content_contacts, contactsFragment);
        ft.commit();
    }

    @Override
    public void showContacts(final List<Contact> contactList) {
        contactsFragment.showContacts(contactList);
    }

    @Override
    public void showSuccessSendMoney() {
        Util.hideKeyboard(this);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        MessageDialog messageDialog = MessageDialog.newInstance(getResources().getString(R.string.success_send_money), this, MessageDialog.TYPE_SUCCESS);
        messageDialog.setCancelable(false);
        messageDialog.show(ft, "dialog");
    }

    @Override
    public void showMinValueError() {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        MessageDialog messageDialog = MessageDialog.newInstance(
                getResources().getString(R.string.msg_error_min_value), null, MessageDialog.TYPE_ERROR);
        messageDialog.setCancelable(false);
        messageDialog.show(ft, "dialog");
        hideLoading();
    }

    @Override
    public void onSelectContact(String contactJson) {
        presenter.setSelectedContact(contactJson);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        sendMoneyDialog = SendMoneyDialog.newInstance(contactJson, this);
        sendMoneyDialog.show(ft, "dialog");
    }

    @Override
    public void onSendMoneyOnClick(String valueStr) {
        sendMoneyDialog.dismiss();
        presenter.sendMoney(valueStr);
    }

    @Override
    public void onModalDialogOKClick() {
        finish();
    }
}
