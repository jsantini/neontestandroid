package com.testneon.ui;

import android.content.Context;

/**
 * Created by jsantini on 02/09/17.
 */

public interface IPresenter {

    void setView(IView view);
    void clearView();
}
