package com.testneon.application;

import android.app.Application;
import android.content.Context;

/**
 * Created by jsantini on 04/09/17.
 */

public class NeonApplication extends Application {

    private static Context mContext;
    private static NeonApplication mInstance;

    public static Context getAppContext() {
        return mContext;
    }

    public static synchronized NeonApplication getInstance() {
        return mInstance;
    }

    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        mInstance = this;
    }
}
